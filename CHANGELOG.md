# 0.2.1

* [`c18dd65`](https://git.mbosch.me/ma27/coredump-exporter/commit/c18dd65e831c8b6aa06812d15fe5bec4f256f9c4): Optimize group-by procedure for coredump-metrics.

# 0.2.0

* [`9e308ca`](https://git.mbosch.me/ma27/coredump-exporter/commit/9e308cafa7afd789ea32938d7a960f25a7c935ee): Replace `coredumps_by_*`-metrics by a single metric named `coredumps` which provides the amount of coredumps grouped by executable and signal-name (e.g. `SIGSEGV`).

# 0.1.2 (2021-06-24)

* [`8eaac32`](https://git.mbosch.me/ma27/coredump-exporter/commit/8eaac3220b3305aaa112295e2b41134fc74b1ffb): Avoid setting bogus `ProtectSystem`-option in service hardening settings.

# 0.1.1 (2021-06-20)

* [`93e1fb0`](https://git.mbosch.me/ma27/coredump-exporter/commit/93e1fb0743c95dd485ee2f9982113327209065b8): Improve performance by avoiding to search through the full journal.

# 0.1.0 (2021-06-20)

Initial release
