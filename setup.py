from os import path
from setuptools import setup


def in_path(key):
    return path.join(
        path.abspath(path.dirname(__file__)),
        key
    )


with open(in_path('README.md'), encoding='utf-8') as f:
    long_description = f.read()

with open(in_path('.version'), encoding='utf-8') as f:
    version = f.read()

setup(
    name='coredump_exporter',
    version=version,
    author='Maximilian Bosch',
    packages=['coredump_exporter'],
    license='MIT',
    long_description=long_description,
    long_description_content_type='text/markdown',
    setup_requires=['nose<2.0'],
    test_suite='nose.collector',
    install_requires=[
        'systemd-python>=234',
        'prometheus-client>=0.9'
    ],
    entry_points={
        'console_scripts': [
            'coredump-exporter = coredump_exporter.__main__:main'
        ]
    }
)
