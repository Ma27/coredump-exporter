{ lib, pkgs, config, ... }:

with lib;

let
  cfg = config.services.coredump-exporter;
in {
  options.services.coredump-exporter = {
    enable = mkEnableOption "coredump-exporter";

    package = mkOption {
      type = types.package;
      default = pkgs.coredump-exporter;
      description = ''
        Package to use.
      '';
    };

    port = mkOption {
      type = types.port;
      default = 9113;
      apply = toString;
      description = ''
        Listen port of the exporter.
      '';
    };
  };

  config = mkIf cfg.enable {
    systemd.services.prometheus-coredump-exporter = {
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      environment.LISTEN_PORT = cfg.port;
      serviceConfig = {
        DynamicUser = true;
        ExecStart = "${cfg.package}/bin/coredump-exporter";
        LockPersonality = "yes";
        PrivateDevices = "yes";
        PrivateMounts = "yes";
        PrivateUsers = "yes";
        ProtectClock = "yes";
        ProtectControlGroups = "yes";
        ProtectHome = "yes";
        ProtectKernelTunables = "yes";
        Restart = "always";
        RestrictAddressFamilies = "AF_UNIX AF_INET AF_INET6";
        RestrictNamespaces = "yes";
        RestrictRealtime = "yes";
        RestrictSUIDSGID = "yes";
        SupplementaryGroups = [ "systemd-journal" ];
        SystemCallArchitectures = "native";
      };
    };
  };

  meta.maintainers = with maintainers; [ ma27 ];
}
