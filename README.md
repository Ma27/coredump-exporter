# coredump-exporter

[![pipeline status](https://img.shields.io/endpoint?url=https://hydra.ist.nicht-so.sexy/job/coredump-exporter/master/x86_64-linux.package/shield)](https://hydra.ist.nicht-so.sexy/job/coredump-exporter/master/x86_64-linux.package)

Prometheus exporter to export metrics of `coredumpctl`.

## Exposed metrics

```
$ curl localhost:9113
# HELP coredumps_total Total number of recorded coredumps on this boot
# TYPE coredumps_total gauge
coredumps_total{boot_id="d0b66454-0f6a-4e8e-ba42-ff27a1f3206b"} 13.0
# HELP coredumps Coredumps grouped by executable and signal
# TYPE coredumps gauge
coredumps{boot_id="d0b66454-0f6a-4e8e-ba42-ff27a1f3206b",exe="/home/ma27/Projects/nix/outputs/out/bin/nix",signal_name="SIGSEGV"} 1.0
coredumps{boot_id="d0b66454-0f6a-4e8e-ba42-ff27a1f3206b",exe="/nix/store/y9wa8j9jqy1b1z4ba2dp1jw18x0j37lv-nix-2.4pre20210601_5985b8b/bin/nix",signal_name="SIGSEGV"} 9.0
coredumps{boot_id="d0b66454-0f6a-4e8e-ba42-ff27a1f3206b",exe="/nix/store/6417c02xz6mfk6v21g3d8df04r92hjg9-systemd-247.6/lib/systemd/systemd-resolved",signal_name="SIGABRT"} 3.0
```

To avoid having too bad performance while waiting for `systemd` to search through
all `journal`-logs, only stats from the current "boot" (i.e. `journalctl -b 0`) will be
exposed and are identifiable by the UUID of the boot.
